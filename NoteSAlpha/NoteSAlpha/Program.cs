﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using labo;

namespace Labo
{

    class Program
    {
        static void Main(string[] args)
        {
            //Dictionary<int, Persona> notes = new Dictionary<int, Persona>();
            string s = "";
            Console.WriteLine("Вас приветствует приложение Note$.Alpha\n" +
                "Для начала давайте создадим вашу первую запись");
            Persona.newnote(ref Persona.notes);
            Console.Clear();

            while (s != "exit")
            {

                Console.WriteLine("Вы можете:\n" +
                    "Создать новую запись - для этого нажмите 1\n" +
                    "Удалить запись - для этого нажмите 2\n" +
                    "Редактировать запись - для этого нажмите 3\n" +
                    "Показать запись - для этого нажмите 4\n" +
                    "Показать все записи в краткой форме - для этого нажмите 5\n" +
                    "Для выхода введите 'exit'\n");
                s = Console.ReadLine();
                switch (s)
                {
                    case "1":
                        Console.Clear();
                        Persona.newnote(ref Persona.notes);
                        break;

                    case "2":
                        Console.Clear();
                        Persona.delnote(ref Persona.notes);
                        break;

                    case "3":
                        Console.Clear();
                        Persona.editnote(ref Persona.notes);
                        break;
                    case "4":
                        Console.Clear();
                        Persona.shownote(ref Persona.notes);
                        break;
                    case "5":
                        Console.Clear();
                        Persona.showall(ref Persona.notes);
                        break;

                    default:
                        break;

                }
                Console.ReadKey();
                Console.Clear();
            }
            Console.WriteLine("До свидания");
            Console.ReadKey();
        }
    }
}
