﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace labo
{
    public class Persona
    {
        public static Dictionary<int, Persona> notes = new Dictionary<int, Persona>();
        public string surname;
        public string name;
        public int phone;
        public string country;
        public DateTime birth; // = new DateTime();
        //public string org;
        private static int Num { get; set; } = 0;
        public static ArrayList trash = new ArrayList();

        public override string ToString()
        {
            return $"Фамилия: {this.surname}\n" +
                $"Имя: {this.name}\n" +
                $"Телефон: {this.phone}\n" +
                $"Страна: {this.country}\n" +
                $"Дата рождения: {this.birth}\n";
            // $"Организация: {this.org}";
        }

        //enum race { };

        public static void newnote(ref Dictionary<int, Persona> notes)
        {

            int num = ++Persona.Num;
            notes.Add(num, new Persona());
            Console.WriteLine("Введите фамилию");
            notes[num].surname = Console.ReadLine();
            Console.WriteLine("Введите имя");
            notes[num].name = Console.ReadLine();
            Console.WriteLine("Введите номер(только цифры)");
            while (true)
            {
                string s = Console.ReadLine();
                if (Int32.TryParse(s, out int k))
                {
                    notes[num].phone = Int32.Parse(s);
                    break;
                }
                else
                {
                    Console.WriteLine("Номер введен не верно, попробуйте снова,\nубрав все символы кроме цифр.");
                }
            }
            Console.WriteLine("Введите страну");
            notes[num].country = Console.ReadLine();
            Console.WriteLine("Введите дату рождения (в формате день.месяц.год)");
            while (true)
            {
                string s = Console.ReadLine();
                if (DateTime.TryParse(s, out DateTime k))
                {
                    notes[num].birth = DateTime.Parse(s);
                    break;
                }
                else
                {
                    Console.WriteLine("Дата введена не верно, попробуйте снова.");
                }
            }
            /*  Console.WriteLine("Введите организацию");
              notes[num].org = Console.ReadLine();*/
        }

        public static void delnote(ref Dictionary<int, Persona> notes)
        {
            Console.WriteLine("Какую запись вы хотите удалить?");
            int n = Int32.Parse(Console.ReadLine());
            notes.Remove(n);
            trash.Add(n);
        }

        public static void editnote(ref Dictionary<int, Persona> notes)
        {
            Console.WriteLine("Есть следующие записи:");
            foreach (KeyValuePair<int, Persona> p in notes)
            {
                Console.WriteLine(p.Key);
                Console.WriteLine(p.Value.surname);
                Console.WriteLine(p.Value.name);
                Console.WriteLine();

            }
            Console.WriteLine("Какую запись будем переделывать?");
            int n = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Мы можем переделать следующие строки(в квадратных скабках указан ключ строки):" +
                "\nФамилия['фамилия' или 1]" +
                "\nИмя['имя' или 2]" +
                "\nТелефон['телефон' или 3]" +
                "\nСтрана['страна' или 4]" +
                "\nДата рождения['др' или 5]" +
                /* "\nОрганизация['организация' или 6]" +*/
                "\n Для отмены напишите 'отмена' или 0");
            string s;

            Console.WriteLine("Введите ключ");
            s = Console.ReadLine();
            switch (s)
            {
                case "фамилия":
                    Console.WriteLine("Введите фамилию");
                    notes[n].surname = Console.ReadLine();
                    break;
                case "1":
                    Console.WriteLine("Введите фамилию");
                    notes[n].surname = Console.ReadLine();
                    break;
                case "имя":
                    Console.WriteLine("Введите имя");
                    notes[n].name = Console.ReadLine();
                    break;
                case "2":
                    Console.WriteLine("Введите имя");
                    notes[n].name = Console.ReadLine();
                    break;

                case "телефон":
                    Console.WriteLine("Введите телефон");
                    try
                    {
                        notes[n].phone = Int32.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        Console.WriteLine("Тут что-то не так");
                    }
                    break;
                case "3":
                    Console.WriteLine("Введите телефон");
                    try
                    {
                        notes[n].phone = Int32.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        Console.WriteLine("Тут что-то не так");
                    }
                    break;
                case "страна":
                    Console.WriteLine("Введите страну");
                    notes[n].country = Console.ReadLine();
                    break;
                case "4":
                    Console.WriteLine("Введите страну");
                    notes[n].country = Console.ReadLine();
                    break;

                case "др":
                    Console.WriteLine("Введите дату рождения");
                    try
                    { notes[n].birth = DateTime.Parse(Console.ReadLine()); }
                    catch
                    { Console.WriteLine("Тут что-то не так"); }
                    break;
                case "5":
                    Console.WriteLine("Введите дату рождения");
                    try
                    { notes[n].birth = DateTime.Parse(Console.ReadLine()); }
                    catch
                    { Console.WriteLine("Тут что-то не так"); }
                    break;

                /* case "организация":
                     Console.WriteLine("Введите фамилию");
                     notes[n].surname = Console.ReadLine();
                     break;
                 case "6":
                     Console.WriteLine("Введите фамилию");
                     notes[n].surname = Console.ReadLine();
                     break;*/
                case "отмена":

                    break;
                case "0":

                    break;

                default:
                    Console.WriteLine("Ключ не опознан, попробуйте заново");
                    break;


            }

        }

        public static void shownote(ref Dictionary<int, Persona> notes)
        {
            Console.WriteLine("Введите номер записи, которую хотите просмотреть");
            int n = Int32.Parse(Console.ReadLine());
            if ((n <= Num) && (n > 0))
            {
                if (trash.Contains(n))
                {
                    Console.WriteLine("Запись была удалена");
                }
                else
                {
                    try
                    {
                        Console.WriteLine(notes[n]);
                    }

                    catch
                    {
                        Console.WriteLine("Если вы получили это сообщение, значит я сделал всё правильно,\n а вы попытались вызвать удаленую строку");
                    }
                }
            }
            else
            {
                Console.WriteLine("Записи с таким номером не найдено");
            }
        }

        public static void showall(ref Dictionary<int, Persona> notes)
        {
            foreach (KeyValuePair<int, Persona> p in notes)
            {
                Console.WriteLine("Запись номер: " + p.Key);
                Console.WriteLine("Фамилия: " + p.Value.surname);
                Console.WriteLine("Имя: " + p.Value.name);
                Console.WriteLine("Телефон: " + p.Value.phone);
                Console.WriteLine();

            }
        }

        /*  public static void showallsup(Persona p)
          {

          }*/

    }
}

